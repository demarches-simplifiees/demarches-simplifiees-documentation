# Documentation Démarches Simplifiées

Documentation **complémentaire** à la documentation officielle de **Démarches Simplifiées**

## Ressources _"officielles"_

* Service en ligne officiel DINSIC : [www.demarches-simplifiees.fr](https://www.demarches-simplifiees.fr/)
* [Suggestions des utilisateurs / utilisatrices](https://demarches-simplifiees.featureupvote.com/)
* [Code source](https://github.com/betagouv/demarches-simplifiees.fr) sur `github.com/betagouv`
* Documentation officielle :
  - [doc.demarches-simplifiees.fr](https://doc.demarches-simplifiees.fr/)
  - [faq.demarches-simplifiees.fr](https://faq.demarches-simplifiees.fr/)
  - [Vidéos : présentations, tutoriels](https://vimeo.com/demarchessimplifiees)
  - [documentation technique](https://github.com/betagouv/demarches-simplifiees.fr/blob/dev/README.md) dans le README du projet

## Documentation complémentaire

- [documentation pour les **utilisateurs** / **utilisatrices**](Utilisateurs/)
  - **administrateurs** : création des **formulaires** et gestion des **instructeurs**
  - **instructeurs** : instruction des **dossiers** déposés par les **usagers** pour une ou plusieurs formulaires
  - **usagers** (personne, association ou entreprise) : création d'un dossier pour un formulaire
- [liste des instances](DevOps/others-DS-instances.md)  du logiciel Démarches-Simplifiées
- [documentation technique](DevOps/)
  - [configurer le **super-admin**](DevOps/app_Super-Admin/) pour la gestion de l'application et l'ajout des **administrateurs**
  - installer l'application
  - [outils en lignes de commande](DevOps/app_CLI/) 
  - les [environnements](DevOps/app_environments/) disponible pour l'application
  - [services externes](DevOps/external_providers/) utilisés par le logiciel
  - pour les développeurs :
    - [lancer les tests](DevOps/developer/tests/)
    - [machine virtuel avec Vagrant](DevOps/developer/vagrant/) 
    - [utiliser les API](DevOps/app_API/)

