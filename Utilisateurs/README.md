# Documentation pour les utilisateurs / utilisatrices


Documentation pour les utilisateurs et les utilisatrices du logiciel _Démarches Simplifiées_.

## Documentation _"officielle"_

- [doc.demarches-simplifiees.fr](https://doc.demarches-simplifiees.fr/)
- [faq.demarches-simplifiees.fr](https://faq.demarches-simplifiees.fr/)
- [Vidéos : présentations, tutoriels](https://vimeo.com/demarchessimplifiees)

## Documentation complémentaire

Documentation **complémentaire** à la documentation _"officielle"_ pour les utilisateurs et les utilisatrices.

### Différents rôles / profils / type de comptes

```yaml
@@@TODO choisir l'appellation à utiliser : "rôle", "profil" et/ou "type de compte"
```
- **administrateurs** : création des **formulaires** et gestion des **instructeurs**
- **instructeurs** : instruction des **dossiers** déposés par les **usagers** pour une ou plusieurs formulaires
- **usagers** (personne, association ou entreprise) : création d'un dossier pour un formulaire

Il existe un 4ème type de compte dit **super-admin** pour la gestion technique du logiciel et à l'ajout des **administrateurs** de démarches. Ce compte **super-admin** est décrit de manière plus précise dans la [documentation technique](../DevOps).
