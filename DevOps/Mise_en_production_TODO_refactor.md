# Mise en production

documentation outdated

## À faire en mode puppet apply

* installer rbenv
* avoir la bonne version de Ruby (TODO demander à Fabien)
* faire l'équivalent du [`BUILD.sh`](https://gitlab.adullact.net/demarches-simplifiees/vagrant-dev-demarches-simplifiees/-/blob/main/BUILD.sh) 

## Points à modifier

### Fichier `.env`

* Fixer le nom de l'application à "tps" `APP_NAME="tps"`
* Déclarer le nom d'hôte `APP_HOST="demarches.example.org"`
* Déclarer les identifiants GitHub (id + secret). [création d'un token Oauth Github](https://developer.github.com/apps/building-oauth-apps/authorizing-oauth-apps/) 


## Personnaliser l'instance de DS

basé sur dev spécifique Adullact (en attente d'une contribution acceptée)

Pour modifier le logo de l'instance apparaissant en haut à gauche de l'écran (dans le header) :

* Ajouter le logo dans le dossier `demarches-simplifiees.fr/app/assets/images`.
* Remplacer par le nom du logo ajouté précédemment le fichier `.env` à la ligne 114, dans `MAIN_LOGO_INSTANCE`.

Pour modifier le nom de l'instance apparaissant à droite du logo en haut de page : 

* Remplacer par le nom souhaité dans le fichier `.env` à la ligne 115 et 116, dans `FULL_INSTANCE_NAME` et `SHORT_INSTANCE_NAME`.


## Question à élucider

### Fichier `.env`

* Comment envoyer des mails sans passer par SendInBlue ?
* Activier LogRage ? `LOGRAGE_ENABLED="disabled"`
