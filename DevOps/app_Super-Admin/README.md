# Super-Admin


Le **super-admin** du logiciel _Démarches-Simplifiées_ peut :
- ajouter des **administrateur** qui ajouterons des démarches (alias: formulaires/procédures)
- modifier ou supprimer un utilisateur (usager, instructeur ou administrateur)
- activer des fonctionnalités supplémentaires (webhook, graphQL, ...)
- consulter l'adresse email utilisée pour remplir un dossier, mais il n'a pas accès aux autres données du formulaire.
- ...

L'authentification par **OTP** est requise pour se connecter en **super-admin**.
- voir [PR #5719 Authentification 2FA pour accéder au manager](https://github.com/betagouv/demarches-simplifiees.fr/pull/5719)
- il est possible de désactiver cette double authentification dans le fichier `.env`.

```bash
# Disabling 2FA for Super-Admins
# SUPER_ADMIN_OTP_ENABLED = "disabled" # "enabled" par défaut
```

## Documentaion

- [Créer le **super-admin**](create_super-admin.md)
- Dans votre navigateur :
  - `manager/` : connection en **super-admin** 
  - `/manager/procedures` : liste des démarches (alias: formulaires/procédures)
  - `/manager/administrateurs` : consultation et ajout d'**administrateur**
  - `/manager/users` : consultation et modification d'un utilisateur 
  - ...
  - `/manager/delayed_job/` :  file d'attente des actions asynchrone (envoi d'email, notification webhook, ...)
  - `/manager/features/features` : activation de fonctionnalités supplémentaires (webhook, graphQL, ...)



### Debug


#### Vérifier la base de données en ligne de commande
```bash
 # Afficher tous les utilisateurs avec droit SuperAdmin
cd <repertoire-de-l-application>
bin/rake superadmin:list 
```

#### Vérifier la base de données en SQL

```bash
# Se connecter à la base de données
psql <databaseName>

```
```sql
-- Afficher tous les utilisateurs existant
SELECT * FROM users;

-- Afficher tous les utilisateurs avec droit SuperAdmin
SELECT *
FROM super_admins;
```



