# Créer le Super-Admin


documentation outdated, see: 
- [#11 - DB - SQL query to list "super-admins" is modified](https://gitlab.adullact.net/demarches-simplifiees/demarches-simplifiees-documentation/-/issues/11)
- [#9 - Update: "super-admin" authentication (OTP 2FA instead of GITHUB)](https://gitlab.adullact.net/demarches-simplifiees/demarches-simplifiees-documentation/-/issues/9)

----------------------

L'authentification par **Github** est requise pour se connecter en **Super-Admin**. 
- L'email Github doit être identique à un utilisateur du logiciel _Démarches-Simplifiées_
- Cet utilisateur du logiciel _Démarches-Simplifiées_ doit avoir le rôle **administrateur**


L'ajout d'un utilisateur super-administrateur se fait en plusieurs étapes :
1. Avoir un compte Github :
   - utilise votre compte Github pour l'environnement de developpement
   - créer un compte Github dédié pour l'environnement de production
2. Sur Github, créer une nouvelle application OAuth
3. Sur le serveur :
   - modifier les variables `GITHUB_CLIENT_ID` et `GITHUB_CLIENT_SECRET` du fichier `.env` de l'application
   - créer un utilisateur avec le rang administrateur, (son adresse mail doit correspondre à l'email du compte github)
   - ajouter cet utilisateur en tant que super-administrateur
4. Dans votre navigateur, connecter vous en super-administrateur via l'authentification Github de la page `/manager/`

### Avoir un compte Github

- Pour l'environnement de production, nous vous conseillons d'utiliser un compte Github dédié.
- L'adresse email du compte Github sera utilisé par l'application. Dans cette documentation, utilisons l'email : `email.github.account@example.com`.


### Sur Github, créer une nouvelle application OAuth

documentation Github : [Creating an OAuth App](https://docs.github.com/en/developers/apps/creating-an-oauth-app)

- menu `Settings > Developer settings > OAuth Apps`
- bouton `New OAuth App`
- remplir  le formulaire avec les informations suivantes :  `(1)`
  - "Homepage URL" : `https://<nom-domaine>/`
  - "Authorization callback URL" : `https://<nom-domaine>/administrations/auth/github/callback`
- quand la nouvelle application OAuth est créer, vous avez maitenant 2 informations à utiliser dans l'étape suivante :
  - **Client ID**
  - **Client Secret**


 `(1)` Pour les développeur, si l'URL de l'application est `http://localhost:3000`, remplir le formulaire avec :
- "Homepage URL" : `http://localhost:3000`
- "Authorization callback URL" : `http://localhost:3000/administrations/auth/github/callback`

### Sur le serveur, modifier le fichier `.env` de l'application

Sur le serveur, modifier les variables `GITHUB_CLIENT_ID` et `GITHUB_CLIENT_SECRET` du fichier `.env` de l'application :

```bash
cd <repertoire-de-l-application>
vim .env
   GITHUB_CLIENT_ID="<Client ID>"
   GITHUB_CLIENT_SECRET="<Client Secret>"
```

Le serveur PUMA doit être redémarré pour que ces variables d'environnement soit pris en compte par l'application.

### Sur le serveur, créer un utilisateur avec le rang administrateur


```yaml
    @@@TODO vérifié si cette étape est vraiment nécessaire
```
Créer un utilisateur avec le rang administrateur, 
son adresse mail doit correspondre à l'email du compte github)

```bash
# créer un utilisateur avec le rang administrateur,
# (son adresse mail doit correspondre à l'email du compte github)
cd <repertoire-de-l-application>
rails console
```
```ruby
    User.create_or_promote_to_administrateur('email.github.account@example.com', 'mot de passe')
    exit
```

### Sur le serveur, ajouter cet utilisateur en tant que super-administrateur
 ```bash
 # Ajouter cet utilisateur en tant que super admin
cd <repertoire-de-l-application>
bin/rake superadmin:list 
bin/rake superadmin:create[email.github.account@example.com]
     Creating Administration for email.github.account@example.com
     email.github.account@example.com created
```

### Connecter vous en super-administrateur 
Dans votre navigateur, connecter vous en super-administrateur via l'authentification Github de la page `/manager/` :
- dans l'environnement de développement : `https://<ip>:3000/manager/`
- sur le serveur de production : `https://<nom-domaine>/manager/`


### Vérification complémenaire pour débug

#### Afficher la configuration de l'application
```bash
cd <repertoire-de-l-application>
cat .env | grep GITHUB
```

#### Vérifier la base de données en ligne de commande
```bash
 # Afficher tous les utilisateurs avec droit SuperAdmin
cd <repertoire-de-l-application>
bin/rake superadmin:list 
```

#### Vérifier la base de données en SQL

```bash
# Se connecter à la base de données
psql <databaseName>

```
```sql
-- Afficher tous les utilisateurs existant
SELECT id, email, instructeur_id, administrateur_id FROM users;

-- Afficher tous les utilisateurs avec droit SuperAdmin
SELECT id, email FROM administrations;
```
