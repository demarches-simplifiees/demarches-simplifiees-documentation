# API du logiciel _Démarches-Simplifiées_

- [Webhook](webhook.md) : notification via une URL de callback
- [API GraphQL](API_GraphQL.md)
  - consultation des dossiers
  - éditions des dossiers

## Projets utilisant les API du logiciel _Démarches-Simplifiées_

- API GraphQL
  - Rails - [_Inspecteur Mes-Démarches_ pour l'instance DS de la _Polynésie Française_](https://github.com/maatinito/inspecteur-mes-demarches)
    - voir : [app/lib/mes_demarches.rb](https://github.com/maatinito/inspecteur-mes-demarches/blob/dev/app/lib/mes_demarches.rb)
  - NodeJs - [tchak/ds-graphql-api-example](https://github.com/tchak/ds-graphql-api-example)
    - plusieurs requêtes type "query" et une requête de "mutation" avec upload de fichier
    - voir : [index.js](https://github.com/tchak/ds-graphql-api-example/blob/master/index.js)      

