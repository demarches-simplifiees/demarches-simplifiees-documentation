# API v2 GraphQL

Cette API GraphQL du logiciel _Démarches-Simplifiées_ permet de :
- consulter :
  - les informations d'une démarche,
  - la liste et les détails des dossiers d'une démarche,
  - les détails d'un dossier spécifique.
- modifier certaines informations d'un dossier : (non testé)
  - Envoyer un message à l'usager d'un dossier;
  - Changer l'état d'un dossier (accepté, refusé, etc.)

## Fonctionnement

- La fonctionnalité est activée par défaut.
- Le **jeton d’identification** de l’API (token) est à récupérer sur la **page de profil** (`/profil`) d'un compte utilisateur avec le rôle "administrateur". 
- L'API est accessible à l'adresse `/api/v2/graphql` (cette URL ne peut pas être consultée dans un navigateur web : code HTTP `404 Page Not Found`). 
- Une fois authentifié en tant qu'administrateur disposant d'un jeton, vous pouvez également accéder à l'**éditeur de requêtes** en ligne  `/graphql` (attention, ne confondez pas cette adresse avec celle de l'endpoint).

## URL à utiliser

- Générer votre jeton pour l’API : `/profil`
- Éditeur de requêtes : `/graphql`
- API (endpoint) :`/api/v2/graphql` <br> 
  nota : cette URL retourne une erreur 404 quand elle est utilisée avec la méthode HTTP GET.


## Ressources
- documentation __officielle__ du logiciel _Démarches-Simplifiées_ :
  - [API GraphQL du logiciel DS](https://doc.demarches-simplifiees.fr/pour-aller-plus-loin/graphql)
  - [schéma de l'API GraphQL](https://demarches-simplifiees-graphql.netlify.app/)
- outils graphiques pour construire les requêtes GraphQL :
  - [GraphQL Playground](https://github.com/prisma-labs/graphql-playground)
  - [GraphiQL](https://github.com/graphql/graphiql)
  - [GraphiQL.app](https://github.com/skevy/graphiql-app) : application electron de **GraphiQL** (pour une mise en œuvre rapide)
- ressources complémentaires :
  - [Introduction aux concepts et raisons d'être de GraphQL](https://blog.octo.com/graphql-et-pourquoi-faire/) (en français)
  - [Documentation officielle de la spécification GraphQL](https://graphql.org/) (en anglais)
  - [4 simple ways to call a GraphQL API](https://www.apollographql.com/blog/graphql/examples/4-simple-ways-to-call-a-graphql-api/) 
  - [How to use GraphQL in php](https://webkul.com/blog/how-to-use-graphql-in-php/) 
    - [webonyx/graphql-php](https://github.com/webonyx/graphql-php)


### Projets utilisant l'API GraphQL du logiciel _Démarches-Simplifiées_

- NodeJs - [tchak/ds-graphql-api-example](https://github.com/tchak/ds-graphql-api-example)
  - plusieurs requêtes type "query" et une requête de "mutation" avec upload de fichier
  - voir : [index.js](https://github.com/tchak/ds-graphql-api-example/blob/master/index.js) 
- Rails - [_Inspecteur Mes-Démarches_ pour l'instance DS de la _Polynésie Française_](https://github.com/maatinito/inspecteur-mes-demarches)
  - voir : [app/lib/mes_demarches.rb](https://github.com/maatinito/inspecteur-mes-demarches/blob/dev/app/lib/mes_demarches.rb) 


```ruby
require 'graphql/client'
require 'graphql/client/http'
...
host = ENV['GRAPHQL_HOST']
puts "server=#{host}"
graphql_url = host + '/api/v2/graphql'
HTTP = GraphQL::Client::HTTP.new(graphql_url) do
  def headers(_context)
    { "Authorization": 'Bearer ' + ENV['GRAPHQL_BEARER'] }
  end
end
pp HTTP
...
Schema = GraphQL::Client.load_schema(HTTP) # Fetch latest schema on init, this will make a network request
Client = GraphQL::Client.new(schema: Schema, execute: HTTP)
Mutation = Client.parse <<-'GRAPHQL'
  mutation EnvoyerMessage($dossierId: ID!, $instructeurId: ID!, $body: String!, $clientMutationId: String) {
      dossierEnvoyerMessage(
          input: {
              dossierId: $dossierId,
              instructeurId: $instructeurId,
              body: $body
              clientMutationId: $clientMutationId,
          }) {
          clientMutationId
          errors {
              message
          }
      }
  }
GRAPHQL

Queries = Client.parse <<-'GRAPHQL'
  query Demarche($demarche: Int!) {
    demarche(number: $demarche) {
      title
      groupeInstructeurs {
        instructeurs {
          id
          email
        }
      }
    }
  }
GRAPHQL
```

## Exemple d'utilisation avec CURL et JQ

### Pré-requis
- `curl`
- `jq`

```bash
sudo apt-get install curl
sudo apt-get install jq
```

### Variables

- Le **jeton d’identification** de l’API (token) est à récupérer sur la **page de profil** d'un compte utilisateur avec le rôle "administration". 
- L'URL du profil est : `https://example.com/profil`

```bash
API_GRAPHQL_URL="https://example.com/api/v2/graphql" 
API_JETON="<YOUR_TOKEN>"
PROCEDURE_ID=2
DOSSIER_ID=4
```

### Requête HTTP `GET` : page d'erreur 404

```bash
# Requête GET : page d'erreur 404
curl -i ${API_GRAPHQL_URL}
        # HTTP/1.1 404 Not Found
        # Content-Type: text/html; charset=iso-8859-1
        # 
        # <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
        # <html><head>
        # <title>404 Not Found</title>
```

### Requête HTTP `POST` (sans jeton et sans paramètre) : erreur au format JSON 

```bash
# Requête POST (sans jeton et sans paramètre) : erreur au format JSON 
curl -i -X POST ${API_GRAPHQL_URL}
        # HTTP/1.1 200 OK
        # Content-Type: application/json; charset=utf-8
        # 
        # {"errors":[{"message":"No query string was present"}]}
```



### Informations concernant une démarche


#### Requête GraphQL
```json
{
  demarche(number: 2) {
    state
    title
    dateCreation
    datePublication
    description
    service {
      typeOrganisme
      organisme
      nom
    }    
  }
}
```


#### Jouer la requête avec CURL
```bash
# Requête à l'API GraphQL de DS
DS_REQUEST="{ demarche(number: ${PROCEDURE_ID}) { state title dateCreation datePublication description service { typeOrganisme organisme nom } } }"
curl -X POST                                       \
     -H "Content-Type: application/json"           \
     -H "Authorization: Bearer token=${API_JETON}" \
     --data "{ \"query\": \"${DS_REQUEST}\" }"     \
     ${API_GRAPHQL_URL} | jq
```

#### Résultat
```json
{
  "data": {
    "demarche": {
      "state": "publiee",
      "title": "Inscription à la Bibliothèque",
      "dateCreation": "2020-07-09T00:49:16+02:00",
      "datePublication": "2020-07-09T01:12:44+02:00",
      "description": "S'inscrire à la bibliothèque de la ville permet...",
      "service": {
        "typeOrganisme": "collectivite_territoriale",
        "organisme": "Mairie de New York",
        "nom": "Service Culture"
      }
    }
  }
}
```

### Informations concernant une démarche avec les dossiers


#### Requête GraphQL
```json
{
  demarche(number: 2) {
    number
    title
    dossiers {
      nodes {
        number
        state
        usager {
          email
        }
      }
    }
  }
}
```


#### Jouer la requête avec CURL
```bash
# Requête à l'API GraphQL de DS
DS_REQUEST="{ demarche(number: ${PROCEDURE_ID}) { title number dossiers { nodes { number state usager { email } } } } } "
curl -X POST                                       \
     -H "Content-Type: application/json"           \
     -H "Authorization: Bearer token=${API_JETON}" \
     --data "{ \"query\": \"${DS_REQUEST}\" }"     \
     ${API_GRAPHQL_URL} | jq
```

#### Résultat
```json
{
  "data": {
    "demarche": {
      "title": "Inscription à la Bibliothèque",
      "number": 2,
      "dossiers": {
        "nodes": [
          {
            "number": 1,
            "state": "en_instruction",
            "usager": {
              "email": "person1@example.com"
            }
          },
          {
            "number": 2,
            "state": "en_construction",
            "usager": {
              "email": "person2@example.com"
            }
          },
          {
            "number": 3,
            "state": "en_instruction",
            "usager": {
              "email": "person3@example.com"
            }
          },
          {
            "number": 4,
            "state": "accepte",
            "usager": {
              "email": "person4@example.com"
            }
          }
        ]
      }
    }
  }
}
```


### Informations concernant un dossier


#### Requête GraphQL
```json
{
  dossier(number: 4) {
    number
    state
    dateDerniereModification
    dateTraitement
    datePassageEnConstruction
    datePassageEnInstruction
    usager {
      email
    }
    champs {
      label
      stringValue
    }
    instructeurs {
      email
    }
  }
}
```


#### Jouer la requête avec CURL
```bash
# Requête à l'API GraphQL de DS
DS_REQUEST="{ dossier(number: ${DOSSIER_ID}) { number state dateDerniereModification dateTraitement datePassageEnConstruction datePassageEnInstruction usager { email } champs { label stringValue } instructeurs { email } } }"
curl -X POST                                       \
     -H "Content-Type: application/json"           \
     -H "Authorization: Bearer token=${API_JETON}" \
     --data "{ \"query\": \"${DS_REQUEST}\" }"     \
     ${API_GRAPHQL_URL} | jq
```

#### Résultat
```json
{
  "data": {
    "dossier": {
      "number": 4,
      "state": "accepte",
      "dateDerniereModification": "2020-07-19T23:16:51+02:00",
      "dateTraitement": "2020-07-19T23:16:44+02:00",
      "datePassageEnConstruction": "2020-07-19T23:01:59+02:00",
      "datePassageEnInstruction": "2020-07-19T23:16:37+02:00",
      "usager": {
        "email": "person4@example.com"
      },
      "champs": [
        {
          "label": "Nom",
          "stringValue": "John Doe"
        },
        {
          "label": "Adresse",
          "stringValue": "1246 Rue Velasquez 31300 Toulouse"
        }
      ],
      "instructeurs": [
        {
          "email": "instructeur@example.com"
        }
      ]
    }
  }
}

```
