# Webhook : notification via une URL de callback

La "notification par lien de rappel HTTP" (aussi appelé « webhook » ou URL de callback) peut être utilisé pour notifier un service tiers du changement de l'état d’un dossier sur une instance du logiciel _demarches-simplifiee_.


Documentation _"officielle"_ : [pour-aller-plus-loin/webhook](https://doc.demarches-simplifiees.fr/pour-aller-plus-loin/webhook) ---> erreur 404
> "L'implémentation courante du webhook n'est pas très résiliente. On n’a pas envie d'encourager son usage. Du coup, nous avons enlevé la doc _"officielle"_."


- La fonctionnalité doit être activée par le **super-admin*** dans `/manager/features/features`.
- Cette fonctionnalité est activée par défault sur l'instance de l'ADULLACT.
- L'URL de callback doit être configurée à la création du formulaire (procédure/démarche) par l'**administrateur** dans le logiciel _demarches-simplifiee_.
- Recommandations de sécurité pour l'URL de callback :
  - URL en HTTPS
  - une URL unique par formulaire (procédure/démarche) pour bien séparer les traitements
  - une URL non prévisible pour éviter que l'URL de callback soit spammée avec des fausses informations
  - un filtrage robuste des données reçues
  - ...

exemple (type POC, non sécurisé) en php :
```php
<?php
    date_default_timezone_set('Europe/Paris');
    if (isset($_SERVER['HTTP_USER_AGENT'])
        && $_SERVER['HTTP_USER_AGENT'] === 'demarches-simplifiees.fr'
        && count($_POST > 0)
    ) {
        $output = '';
        $output .= "--- Webhook DS Adullact ------------------------------\n";
        $output .= date("Y-m-d H:i:s") ."\n";
        foreach ($_POST as $key => $value){
            $keyClean = htmlentities(strip_tags($key), ENT_QUOTES);
            $valueClean = htmlentities(strip_tags($value), ENT_QUOTES);
            $output .= "POST ['$keyClean'] => '$valueClean'\n";
        }
        $output .= "\n\n";
        file_put_contents("log_DS_webhook.log", $output,  FILE_APPEND);
//      mail('example@example.com', 'Webhook DS - TEST ', $output);
    } else {
        header("HTTP/1.0 418 I'm A Teapot");
        echo "HTTP 418 - I'm A Teapot";
    }

/*  ------------------------------
    array(4) {
      ["procedure_id"]=>  string(1) "2"
      ["dossier_id"]=>  string(1) "1"
      ["state"]=>  string(15) "en_construction"
      ["updated_at"]=>  string(25) "2020-07-19 22:54:56 +0200"
    }
    Array(
        [procedure_id] => 2
        [dossier_id] => 1
        [state] => en_construction
        [updated_at] => 2020-07-19 22:54:56 +0200
    )
    ------------------------------
    Array(
        [procedure_id] => 2
        [dossier_id] => 1
        [state] => en_instruction
        [updated_at] => 2020-07-19 22:51:52 +0200
    )
    ---------------------------------------------
    array(4) {
      ["procedure_id"]=>  string(1) "2"
      ["dossier_id"]=>  string(1) "1"
      ["state"]=>  string(7) "accepte"
      ["updated_at"]=>  string(25) "2020-07-19 22:56:05 +0200"
    }
    Array(
        [procedure_id] => 2
        [dossier_id] => 1
        [state] => accepte
        [updated_at] => 2020-07-19 22:56:05 +0200
    )
    ---------------------------------------------
    Array(
        [procedure_id] => 2
        [dossier_id] => 1
        [state] => sans_suite
        [updated_at] => 2020-07-19 22:57:09 +0200
    )
    ---------------------------------------------
    Array(
        [procedure_id] => 2
        [dossier_id] => 1
        [state] => refuse
        [updated_at] => 2020-07-19 22:57:52 +0200
    )
    ------------------------------  */


```
