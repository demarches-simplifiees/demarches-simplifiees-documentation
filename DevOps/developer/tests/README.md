# Tests

## Exécuter tous les tests

```yaml
  @@@TODO préciser la différence entre ces 3 méthodes
```

3 méthodes pour exécuter les tests :
```bash
cd <path_demarchesSimplifiees>

# Run all spec files 
bin/rake spec       # methode 1 
bin/rspec           # methode 2 - Run all spec files (i.e., those matching spec/**/*_spec.rb)
bundle exec rspec   # methode 3 - run all spec files (i.e., those matching spec/**/*_spec.rb)

# Display group and example names
bin/rspec --format documentation

# List slowest examples (default: 10)
bin/rspec --profile
bin/rspec --profile 10
```

## Filtrer les tests à exécuter

```bash
cd <path_demarchesSimplifiees>

# Run all spec files in a single directory (recursively)
bin/rspec path/to/

# Run a single spec file
bin/rspec path/to/a_spec.rb

# Run a single example from a spec file (by line number)
bin/rspec path/to/a_spec.rb:37

# Pass example ids enclosed in square brackets
rspec path/to/a_spec.rb[1:5,1:6] # run the 5th and 6th examples/groups defined in the 1st group


# Files matching pattern
########################

# Load files matching pattern (default: "spec/**/*_spec.rb")
bin/rspec                    spec/controllers/users/
bin/rspec -P                "spec/controllers/users/*"
bin/rspec --pattern         "spec/controllers/users/*"

# Load files except those matching pattern. Opposite effect of --pattern.
bin/rspec --exclude-pattern "spec/controllers/users/*"

# Combine --pattern and --exclude-pattern
bin/rspec --pattern         "spec/controllers/users/*"  \
          --exclude-pattern "spec/controllers/users/sessions*"


# Description matching pattern 
##############################
bin/rspec -e  "france connect"
        # -e, --example         STRING    Run examples whose full nested names include STRING 
        # -E, --example-matches REGEX     Run examples whose full nested names match REGEX 
```

## Relancer uniquement les tests qui ont échoué précédemment
```
bin/rspec                                               # lancer tous les tests
bin/rspec --only-failures                               # relancer uniquement tous les tests en échec
bin/rspec --next-failure                                # relancer uniquement le 1er test en échec
bin/rspec --only-failures file_path/file_name_spec.rb   # relancer uniquement les tests en échec d'un fichier
bin/rspec --next-failure  file_path/file_name_spec.rb   # relancer uniquement le 1er test en échec d'un fichier

# Pour vider l'historique des tests en échec, il faut supprimer le fichier failing_specs.txt
rm -v failing_specs.txt
```

## Ressources

- [RSpec](https://rspec.info)
- [RSpec command line](https://relishapp.com/rspec/rspec-core/v/3-9/docs/command-line)
- [RSpec Rail](https://rubydoc.info/gems/rspec-rails/)

### Documentation _officielle_ du logiciel

[Exécution des tests (RSpec)](https://github.com/betagouv/demarches-simplifiees.fr/blob/dev/README.md#ex%C3%A9cution-des-tests-rspec)

> Les tests ont besoin de leur propre base de données et certains d'entre eux utilisent Selenium pour s'exécuter dans un navigateur. N'oubliez pas de créer la base de test et d'installer chrome et chromedriver pour exécuter tous les tests.
> 
> Pour exécuter les tests de l'application, plusieurs possibilités :
> 
> - Lancer tous les tests
> 
>         bin/rake spec
>         bin/rspec
> 
> - Lancer un test en particulier
> 
>         bin/rake spec SPEC=file_path/file_name_spec.rb:line_number
>         bin/rspec file_path/file_name_spec.rb:line_number
> 
> - Lancer tous les tests d'un fichier
> 
>         bin/rake spec SPEC=file_path/file_name_spec.rb
>         bin/rspec file_path/file_name_spec.rb



### CI CircleCI

[.circleci/config.yml](https://github.com/betagouv/demarches-simplifiees.fr/blob/dev/.circleci/config.yml)

```bash
cd <path_demarchesSimplifiees>
bundle exec rspec --profile 10 --format RspecJunitFormatter --out ~/test_results/rspec.xml --format progress
```


## Erreurs courantes


### Erreur : "must be owner of database tps_test"

```bash
cd <path_demarchesSimplifiees>
bin/rspec
    rails aborted!
    ActiveRecord::StatementInvalid: PG::InsufficientPrivilege: ERROR:  must be owner of database tps_test
```

#### Solution

Solution temporaire utilisé dans le vagrant Adullact

> Si vous n'avez jamais lancé les tests, il y a une manipulation à faire en plus (une seule fois).
> Étape manuelle à faire tant que [la tâche identifiée dans cette issue](https://gitlab.adullact.net/demarches-simplifiees/vagrant-dev-demarches-simplifiees/-/issues/15#note_57672) n'est pas remplit : 

```bash
cd <path_demarchesSimplifiees>
sudo su postgres
psql
```

```sql
    ALTER USER tps_test WITH SUPERUSER;
    \q
```
