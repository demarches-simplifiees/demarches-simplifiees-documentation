
# DS Vagrant - Step 1: Prerequisites and build Vagrant box

see: [vagrant-demarches-simplifiees](https://gitlab.adullact.net/demarches-simplifiees/vagrant-dev-demarches-simplifiees) repository.

## Prerequisites

- [Vagrant](https://www.vagrantup.com/downloads)
- [VirtualBox](https://www.virtualbox.org/)
- Git
- a Ruby version management tool like RVM or [RBenv](https://github.com/rbenv/rbenv)
- r10k (`gem install r10k`)  

For Linux users (debian, ubuntu), do *not* install Vagrant and VirtualBox from `apt-get` (packages are way too old),
download `.deb` from the respective websites.

## Usage

### 1. Grab source code of Démarches Simplifiées

```bash
mkdir my-project # "my-project" is the parent directory holding DS source code
cd my-project   
git clone https://github.com/betagouv/demarches-simplifiees.fr.git
```

Note: you may clone your own fork of the source code (if you want to contribute for instance)

### 2. Grab source code of vagrant-demarches-simplifiees 

```bash 
git clone https://gitlab.adullact.net/demarches-simplifiees/vagrant-dev-demarches-simplifiees.git
```

### 3. Prepare the vagrant box

```bash
cd vagrant-dev-demarches-simplifiees
./BUILD.sh
```

### 4. Run the vagrant box

```bash
vagrant up
```

### 5. Finish manual configuration

Complete the instructions from [Last manual steps for installation of Démarches simplifiées ](step2_configuration_last_manual_steps.md).

### How to rebuild the vagrant box 

```bash
vagrant destroy -f && vagrant up
```
