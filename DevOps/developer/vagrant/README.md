# Vagrant box for _Démarches-Simplifiées_

Vagrant box for Démarches Simplifiées (DS)

see: [vagrant-demarches-simplifiees](https://gitlab.adullact.net/demarches-simplifiees/vagrant-dev-demarches-simplifiees) repository:
- [Step 1: Prerequisites and build Vagrant box](step1_Prerequisites-and-Install.md)
- [Step 2: Last manual steps for installation of Démarches simplifiées ](step2_configuration_last_manual_steps.md)
- [Running Tests In the VM](vagrant_test.md)

## Restart vagrant box

if you stopped this Vagrant box with the `vagrant halt` command (turning off your computer is the same), you can restart your vagrant box by following the instructions below:

```bash
cd vagrant-demarches-simplifiees
vagrant up
vagrant ssh
``` 

### In vagrant box

```bash
cd /home/vagrant/src/

# Update dependencies and database
bin/update # useful if you have updated the DS repository   

# Start server to be able to restart it quickly
RAILS_QUEUE_ADAPTER=delayed_job
bin/rails server --binding=0.0.0.0
        # Use Ctrl-C to stop
``` 


## Use Démarches-Simplifiées

### As anonymous user

In your browser, type in: `http://localhost:3000/` 

### As registered user

1. Go to `http://localhost:3000/users/sign_in`
2. Use this user and this password:
   - `test@exemple.fr`
   - `this is a very complicated password !`
3. Check user's emails at `http://localhost:3000/letter_opener`
4. Validate registration 

`test@exemple.fr` user has following roles: 
- user
- instructor
- administrator
- super-admin

### As a _Super-Admin_

2. Go to `http://localhost:3000/manager/`
2. Use this super-admin email and this password:
   - `test@exemple.fr`
   - `this is a very complicated password !`
2. To enable features, go to `http://localhost:3000/manager/features/`
