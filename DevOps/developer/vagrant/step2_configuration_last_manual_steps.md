# DS Vagrant - Step 2: Last manual steps for installation of Démarches simplifiées

## 1. Get into the box

Once the vagrant box is build:

```bash
vagrant ssh
```

## 2. Run install commands

```bash
cd /home/vagrant/src/
yarn install    # (1) inclus dans bin/setup
bundle install  # (2) inclus dans bin/setup
bin/setup       # (3) execute aussi "yarn install" et "bundle install"
```
Les étapes (1) et (2) ne sont pas obligatoire, elles servent juste à diminuer la durée de l'étape (3).

voir : [/bin/setup](https://github.com/betagouv/demarches-simplifiees.fr/blob/dev/bin/setup)

## 3. Run server

```bash
# option 1 : start server in daemon mode
/home/vagrant/src/bin/rails server --daemon --binding=0.0.0.0
/home/vagrant/src/bin/delayed_job run &

# option 2 : start server to be able to restart it quickly
RAILS_QUEUE_ADAPTER=delayed_job
/home/vagrant/src/bin/rails server --binding=0.0.0.0
        # Use Ctrl-C to stop
``` 

In case of error of previous commands, run:

```bash
echo 'export PATH=$PATH:/home/ds/rbenv/shims:/home/ds/rbenv/bin' >> /home/ds/.bashrc
```

And relaunch terminal so that changes be taken into account, then re-type last two commands.

## 4. Use Démarches-Simplifiées

### As anonymous user

In your browser, type in: `http://localhost:3000/` 

### As registered user

1. Go to `http://localhost:3000/users/sign_in`
2. Use this user and this password:
   - `test@exemple.fr`
   - `this is a very complicated password !`
3. Check user's emails at `http://localhost:3000/letter_opener`
4. Validate registration 

`test@exemple.fr` user has following roles: 
- user
- instructor
- administrator
- super-admin

### As a _Super-Admin_

2. Go to `http://localhost:3000/manager/`
2. Use this super-admin email and this password (OTP code is not necessary, as it is disabled in this Vagrant):
   - `test@exemple.fr`
   - `this is a very complicated password !`
2. To enable features, go to `http://localhost:3000/manager/features/`
