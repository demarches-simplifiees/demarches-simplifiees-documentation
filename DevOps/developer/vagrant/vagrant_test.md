# Running Tests In the VM

voir la [documentation sur les tests](../tests) du logiciels _démarches-simplifiées_

## Étape manuelle préalable aux tests

Si vous n'avez jamais lancé les tests, il y a une manipulation à faire en plus (une seule fois).
Étape manuelle à faire tant que [la tâche identifiée dans cette issue](https://gitlab.adullact.net/demarches-simplifiees/vagrant-dev-demarches-simplifiees/-/issues/15#note_57672) n'est pas remplit : 


```bash
cd <path_demarchesSimplifiees>
sudo su postgres
psql
```

```sql
    ALTER USER tps_test WITH SUPERUSER;
    \q
```

