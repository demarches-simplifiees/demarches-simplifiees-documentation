
# Linters

The project uses [several linters](https://github.com/betagouv/demarches-simplifiees.fr/blob/dev/lib/tasks/lint.rake) to check the readability and quality of the code.

- **RuboCop** - Ruby static code analyzer (a.k.a. linter) and code formatter
- **HAML-Lint** - Linter tool for validating HAML files (views)
- **SCSS-Lint** - Linter tool for validating SCSS file (CSS stylesheets)
- **Brakeman** - Static analysis tool which checks Ruby on Rails applications for security vulnerabilities
- **ECLint** - Linter tool for validating code that doesn't adhere to settings defined in **.editorconfig**
- **ESLint** - Linter tool for identifying and reporting on patterns in **JavaScript**

Run all linters: 
```bash
cd <path_demarchesSimplifiees>
bin/rake lint

# equivalent to:
bundle exec rubocop
bundle exec haml-lint app/views/
bundle exec scss-lint app/assets/stylesheets/
bundle exec brakeman --no-pager
yarn lint:ec  # ECLint 
yarn lint:js  # ESLint
```

