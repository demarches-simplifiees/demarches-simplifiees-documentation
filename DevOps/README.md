# Documentation Technique


## Ressources _"officielles"_

* [Code source](https://github.com/betagouv/demarches-simplifiees.fr) sur `github.com/betagouv` avec gestion des contributions 
* Documentation officielle :
  - [doc.demarches-simplifiees.fr](https://doc.demarches-simplifiees.fr/)
  - [faq.demarches-simplifiees.fr](https://faq.demarches-simplifiees.fr/)
  - [Vidéos : présentations, tutoriels](https://vimeo.com/demarchessimplifiees)
  - [documentation technique](https://github.com/betagouv/demarches-simplifiees.fr/blob/dev/README.md) dans le README du projet

## Documentation complémentaire

Documentation **complémentaire** à la documentation officielle de **Démarches Simplifiées**
- [configurer le **super-admin**](app_Super-Admin/) pour la gestion de l'application et l'ajout des **administrateurs**
- installer l'application
- [environnements](app_environments/) de l'application (`production`, `development`, `test`)
- [outils en lignes de commande](app_CLI/) : 
  - [bin/rake](app_CLI/cli_rake.md)
  - [bin/rails](app_CLI/cli_rails.md)
  - [bin/rails console](app_CLI/cli_rails-console.md)
- [services externes](external_providers/) utilisés par le logiciel
- pour les développeurs :
  - [Tests](developer/tests/)
  - [Linters](developer/linters/)
  - [Vagrant](developer/vagrant/) 
- [utiliser les API](app_API/)
  - [Webhook](app_API/webhook.md) : notification via une URL de callback
  - [API v2 GraphQL](app_API/API_GraphQL.md) 
    - consultation des dossiers
    - éditions des dossiers





