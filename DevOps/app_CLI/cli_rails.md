# bin/rails

voir aussi :
- [bin/rake](cli_rake.md)
- [bin/rails console](cli_rails-console.md)

## Documentation
- [Rails Command Line](https://guides.rubyonrails.org/command_line.html)

## Commmand line

```bash
cd <path_demarchesSimplifiees>

# Generic
########################################################
bin/rails -v           # Display rails version
bin/rails -h           # Help: options are ...
bin/rails -T [PATTERN] # Display the tasks (matching optional PATTERN) with descriptions
bin/rails -D [PATTERN] # Describe the tasks (matching optional PATTERN)
bin/rails stats  # statistics on applicaiton code: thousands of lines of code, code to test ratio, ...
bin/rails about  # Version numbers for Ruby, RubyGems, Rails, 
                 # application's folder, current Rails environment name, database adapter, schema version

# Starting Rails server
bin/rails server -d        | --daemon            # Runs server as a Daemon
bin/rails server -p <port> | --port=<port>       # Runs Rails on the specified port - defaults to 3000.
bin/rails server -b <ip>   | --binding=<ip>      # Binds Rails to specified IP, 
                                                 # defaults to 'localhost' (development) and '0.0.0.0' in other environments'
bin/rails server -e <env>  | --environment=<env> # Specifies the environment to run server
bin/rails server -e production     # starting server under "production" environment
bin/rails server -e development    # starting server under "development" environment
bin/rails server -e test           # starting server under "test" environment

# List defined routes
bin/rails routes    # list all defined routes
  -c, [--controller=CONTROLLER]      # Filter by a specific controller, e.g. PostsController or Admin::PostsController.
  -g, [--grep=GREP]                  # Grep routes by a specific pattern.
  -E, [--expanded], [--no-expanded]  # Print routes expanded vertically with parts explained.

# Searches through code for comments beginning with a specific keyword
rails notes # by default, it will search for FIXME, OPTIMIZE, and TODO



# All tasks
########################################################
rails about                                                           # List versions of all Rails frameworks and the environment
rails action_mailbox:ingress:exim                                     # Relay an inbound email from Exim to Action Mailbox (URL and INGRESS_P...
rails action_mailbox:ingress:postfix                                  # Relay an inbound email from Postfix to Action Mailbox (URL and INGRES...
rails action_mailbox:ingress:qmail                                    # Relay an inbound email from Qmail to Action Mailbox (URL and INGRESS_...
rails action_mailbox:install                                          # Copy over the migration
rails action_text:install                                             # Copy over the migration, stylesheet, and JavaScript files
rails active_storage:install                                          # Copy over the migration needed to the application
rails after_party:add_default_skip_validation_to_piece_justificative  # Deployment task: add_default_skip_validation_to_piece_justificative
rails after_party:add_traitements_from_dossiers                       # Deployment task: add_traitements_from_dossiers
rails after_party:cleanup_deleted_dossiers                            # Deployment task: cleanup_deleted_dossiers
rails after_party:drop_down_list_options_to_json                      # Deployment task: drop_down_list_options_to_json
rails after_party:fix_champ_etablissement                             # Deployment task: fix_champ_etablissement
rails after_party:fix_cloned_revisions                                # Deployment task: fix_cloned_revisions
rails after_party:fix_dossier_etablissement                           # Deployment task: fix_dossier_etablissement
rails after_party:migrate_revisions                                   # Deployment task: migrate_revisions
rails after_party:migrate_service_organisme                           # Deployment task:  migrate service organisme
rails after_party:process_expired_dossiers_en_construction            # Deployment task: process_expired_dossiers_en_construction
rails after_party:run                                                 # runs (in order) all pending after_party deployment tasks, if they hav...
rails after_party:status                                              # Check the status of after_party deployment tasks
rails app:template                                                    # Applies the template supplied by LOCATION=(/path/to/template) or URL
rails app:update                                                      # Update configs and some other initially generated files (or use just ...
rails assets:clean[keep]                                              # Remove old compiled assets
rails assets:clobber                                                  # Remove compiled assets
rails assets:environment                                              # Load asset compile environment
rails assets:precompile                                               # Compile all the assets named in config.assets.precompile
rails cache_digests:dependencies                                      # Lookup first-level dependencies for TEMPLATE (like messages/show or c...
rails cache_digests:nested_dependencies                               # Lookup nested dependencies for TEMPLATE (like messages/show or commen...
rails db:create                                                       # Creates the database from DATABASE_URL or config/database.yml for the...
rails db:drop                                                         # Drops the database from DATABASE_URL or config/database.yml for the c...
rails db:environment:set                                              # Set the environment value for the database
rails db:fixtures:load                                                # Loads fixtures into the current environment's database
rails db:migrate                                                      # Migrate the database (options: VERSION=x, VERBOSE=false, SCOPE=blog)
rails db:migrate:status                                               # Display status of migrations
rails db:prepare                                                      # Runs setup if database does not exist, or runs migrations if it does
rails db:rollback                                                     # Rolls the schema back to the previous version (specify steps w/ STEP=n)
rails db:schema:cache:clear                                           # Clears a db/schema_cache.yml file
rails db:schema:cache:dump                                            # Creates a db/schema_cache.yml file
rails db:schema:dump                                                  # Creates a db/schema.rb file that is portable against any DB supported...
rails db:schema:load                                                  # Loads a schema.rb file into the database
rails db:seed                                                         # Loads the seed data from db/seeds.rb
rails db:seed:replant                                                 # Truncates tables of each database for current environment and loads t...
rails db:setup                                                        # Creates the database, loads the schema, and initializes with the seed...
rails db:structure:dump                                               # Dumps the database structure to db/structure.sql
rails db:structure:load                                               # Recreates the databases from the structure.sql file
rails db:version                                                      # Retrieves the current schema version number
rails fix_timestamps_of_migrated_dossiers:run                         # Fix the timestamps of dossiers affected by the faulty PJ migration
rails geocode:all                                                     # Geocode all objects without coordinates
rails geocoder:maxmind:geolite:download                               # Download MaxMind GeoLite City data
rails geocoder:maxmind:geolite:extract                                # Extract (unzip) MaxMind GeoLite City data
rails geocoder:maxmind:geolite:insert                                 # Load/refresh MaxMind GeoLite City data
rails geocoder:maxmind:geolite:load                                   # Download and load/refresh MaxMind GeoLite City data
rails graphql:pro:validate[gem_version]                               # Get the checksum of a graphql-pro version and compare it to published...
rails graphql:schema:dump                                             # Dump the schema to JSON and IDL
rails graphql:schema:idl                                              # Dump the schema to IDL in app/graphql/schema.graphql
rails graphql:schema:json                                             # Dump the schema to JSON in app/graphql/schema.json
rails haml:erb2haml                                                   # Convert html.erb to html.haml each file in app/views
rails jobs:check[max_age]                                             # Exit with error status if any jobs older than max_age seconds haven't...
rails jobs:clear                                                      # Clear the delayed_job queue
rails jobs:display_schedule                                           # Display schedule for all cron jobs
rails jobs:schedule                                                   # Schedule all cron jobs
rails jobs:work                                                       # Start a delayed_job worker
rails jobs:workoff                                                    # Start a delayed_job worker and exit when all available jobs are complete
rails log:clear                                                       # Truncates all/specified *.log files in log/ to zero bytes (specify wh...
rails middleware                                                      # Prints out your Rack middleware stack
rails raven:test[dsn]                                                 # Send a test event to the remote Sentry server
rails restart                                                         # Restart app by touching tmp/restart.txt
rails secret                                                          # Generate a cryptographically secure secret key (this is typically use...
rails spec                                                            # Run all specs in spec directory (excluding plugin specs)
rails spec:controllers                                                # Run the code examples in spec/controllers
rails spec:features                                                   # Run the code examples in spec/features
rails spec:helpers                                                    # Run the code examples in spec/helpers
rails spec:jobs                                                       # Run the code examples in spec/jobs
rails spec:lib                                                        # Run the code examples in spec/lib
rails spec:mailers                                                    # Run the code examples in spec/mailers
rails spec:middlewares                                                # Run the code examples in spec/middlewares
rails spec:models                                                     # Run the code examples in spec/models
rails spec:policies                                                   # Run the code examples in spec/policies
rails spec:serializers                                                # Run the code examples in spec/serializers
rails spec:services                                                   # Run the code examples in spec/services
rails spec:views                                                      # Run the code examples in spec/views
rails stats                                                           # Report code statistics (KLOCs, etc) from the application or engine
rails superadmin:create[email]                                        # Create a new super-admin account with the #EMAIL email address
rails superadmin:delete[email]                                        # Delete the #EMAIL super-admin account
rails superadmin:list                                                 # List all super-admins
rails support:change_user_email                                       # Change a user’s mail from OLD_EMAIL to NEW_EMAIL
rails support:delete_user_account                                     # Delete the user account for a given USER_EMAIL on the behalf of ADMIN...
rails support:update_dossier_siret                                    # Change the SIRET for a given dossier (specified by DOSSIER_ID)
rails test                                                            # Runs all tests in test folder except system ones
rails test:db                                                         # Run tests quickly, but also reset db
rails test:system                                                     # Run system tests only
rails time:zones[country_or_offset]                                   # List all time zones, list by two-letter country code (`rails time:zon...
rails tmp:clear                                                       # Clear cache, socket and screenshot files from tmp/ (narrow w/ tmp:cac...
rails tmp:create                                                      # Creates tmp directories for cache, sockets, and pids
rails webpacker                                                       # Lists all available tasks in Webpacker
rails webpacker:binstubs                                              # Installs Webpacker binstubs in this application
rails webpacker:check_binstubs                                        # Verifies that webpack & webpack-dev-server are present
rails webpacker:check_node                                            # Verifies if Node.js is installed
rails webpacker:check_yarn                                            # Verifies if Yarn is installed
rails webpacker:clean[keep,age]                                       # Remove old compiled webpacks
rails webpacker:clobber                                               # Remove the webpack compiled output directory
rails webpacker:compile                                               # Compile JavaScript packs using webpack for production with digests
rails webpacker:info                                                  # Provide information on Webpacker's environment
rails webpacker:install                                               # Install Webpacker in this application
rails webpacker:install:angular                                       # Install everything needed for Angular
rails webpacker:install:coffee                                        # Install everything needed for Coffee
rails webpacker:install:elm                                           # Install everything needed for Elm
rails webpacker:install:erb                                           # Install everything needed for Erb
rails webpacker:install:react                                         # Install everything needed for React
rails webpacker:install:stimulus                                      # Install everything needed for Stimulus
rails webpacker:install:svelte                                        # Install everything needed for Svelte
rails webpacker:install:typescript                                    # Install everything needed for Typescript
rails webpacker:install:vue                                           # Install everything needed for Vue
rails webpacker:verify_install                                        # Verifies if Webpacker is installed
rails webpacker:yarn_install                                          # Support for older Rails versions
rails yarn:install                                                    # Install all JavaScript dependencies as specified via Yarn
rails zeitwerk:check                                                  # Checks project structure for Zeitwerk compatibility

 ```                

 
