# bin/rake

## Ressources
- [Rails Command Line - Rake](https://guides.rubyonrails.org/v4.2/command_line.html#rake)
- [Since Rails 5.0+ has rake commands built into the rails executable, bin/rails is the new default for running commands](https://guides.rubyonrails.org/v5.2/command_line.html#bin-rails)
  - but `bin/rails <task>` is slower than `bin/rake <task>`


## Commmand line

```bash
cd <path_demarchesSimplifiees>

# Generic
########################################################
bin/rake -V           # Display rake version
bin/rake -h           # Help: rake options are ...
bin/rake -T [PATTERN] # Display the tasks (matching optional PATTERN) with descriptions
bin/rake -D [PATTERN] # Describe the tasks (matching optional PATTERN)

# Rails tasks
########################################################
bin/rake about      # List versions of all Rails frameworks and the environment
bin/rake routes     # Print out all defined routes in match order, with names
bin/rake tmp:clear  # Clear cache, socket and screenshot files from tmp/ (narrow w/ tmp:cache:clear, tmp:sockets:clear, tmp:screenshots:clear)
bin/rake log:clear  # Truncates all/specified *.log files in log/ to zero bytes


# Specific Démarches-Simplifiées tasks
########################################################

# List, add and delete super-admin account
bin/rake -D superadmin:            # Help - List of available options
bin/rake superadmin:list           # List all super-admins
bin/rake superadmin:delete[email]  # Delete the #EMAIL super-admin account
bin/rake superadmin:create[email]  # Create a new super-admin account with the #EMAIL email address

# Support task
bin/rake -D support:                   # Help - List of available options
bin/rake support:change_user_email     # Change a user’s mail from OLD_EMAIL to NEW_EMAIL
bin/rake support:delete_user_account   # Delete the user account for a given USER_EMAIL on the behalf of ADMIN_EMAIL
bin/rake support:update_dossier_siret  # Change the SIRET for a given dossier (specified by DOSSIER_ID)

# Rails specs tasks (dev)
########################################################
bin/rake spec                # Run all specs in spec directory (excluding plugin specs)
bin/rake spec:controllers    # Run the code examples in spec/controllers
bin/rake spec:features       # Run the code examples in spec/features
bin/rake spec:helpers        # Run the code examples in spec/helpers
bin/rake spec:jobs           # Run the code examples in spec/jobs
bin/rake spec:lib            # Run the code examples in spec/lib
bin/rake spec:mailers        # Run the code examples in spec/mailers
bin/rake spec:middlewares    # Run the code examples in spec/middlewares
bin/rake spec:models         # Run the code examples in spec/models
bin/rake spec:policies       # Run the code examples in spec/policies
bin/rake spec:serializers    # Run the code examples in spec/serializers
bin/rake spec:services       # Run the code examples in spec/services
bin/rake spec:views          # Run the code examples in spec/views


# All tasks
########################################################
bin/rake about                                                # List versions of all Rails frameworks and the environment
bin/rake action_text:install                                  # Copy over the migration, stylesheet, and JavaScript files
bin/rake active_storage:install                               # Copy over the migration needed to the application
bin/rake after_party:add_missing_dossier_id_to_repetitions    # Deployment task: add_missing_dossier_id_to_repetitions
bin/rake after_party:archivee_to_close                        # Deployment task: archivee_to_close
bin/rake after_party:clean_procedure_presentation_from_followers_gestionnaires  # Deployment task: clean_procedure_presentation_from_followers_gestionnaires
bin/rake after_party:cleanup_deleted_dossiers                 # Deployment task: cleanup_deleted_dossiers
bin/rake after_party:create_default_groupe_instructeur        # Deployment task: create_default_groupe_instructeur
bin/rake after_party:create_default_path_for_brouillons       # Deployment task: create_default_path_for_brouillons
bin/rake after_party:enable_export_purge                      # Deployment task: enable_export_purge
bin/rake after_party:enable_seek_and_destroy_job              # Deployment task: enable_seek_and_destroy_job
bin/rake after_party:fix_champ_etablissement                  # Deployment task: fix_champ_etablissement
bin/rake after_party:fix_dossier_etablissement                # Deployment task: fix_dossier_etablissement
bin/rake after_party:fix_macedonia                            # Deployment task: fix_macedonia
bin/rake after_party:link_assign_and_groupe_instructeur       # Deployment task: link_assign_and_groupe_instructeur
bin/rake after_party:migrate_mail_body_to_actiontext          # Deployment task: migrate_mail_body_to_actiontext
bin/rake after_party:migrate_service_organisme                # Deployment task:  migrate service organisme
bin/rake after_party:populate_user_administrateur_ids         # Deployment task: populate_user_administrateur_ids
bin/rake after_party:populate_user_instructeur_ids            # Deployment task: populate_user_instructeur_ids
bin/rake after_party:process_expired_dossiers_en_construction # Deployment task: process_expired_dossiers_en_construction
bin/rake after_party:rename_active_storage_attachments        # Deployment task: rename_active_storage_attachments
bin/rake after_party:run                                      # runs (in order) all pending after_party deployment tasks, if they have not run yet against the curr...
bin/rake after_party:set_declarative_procedures               # Deployment task: set_declarative_procedures
bin/rake after_party:status                                   # Check the status of after_party deployment tasks
bin/rake after_party:update_admin_last_sign_in_at             # Deployment task: update_admin_last_sign_in_at
bin/rake app:template                                         # Applies the template supplied by LOCATION=(/path/to/template) or URL
bin/rake app:update                                           # Update configs and some other initially generated files (or use just update:configs or update:bin)
bin/rake assets:clean[keep]                                   # Remove old compiled assets
bin/rake assets:clobber                                       # Remove compiled assets
bin/rake assets:environment                                   # Load asset compile environment
bin/rake assets:precompile                                    # Compile all the assets named in config.assets.precompile
bin/rake cache_digests:dependencies                           # Lookup first-level dependencies for TEMPLATE (like messages/show or comments/_comment.html)
bin/rake cache_digests:nested_dependencies                    # Lookup nested dependencies for TEMPLATE (like messages/show or comments/_comment.html)
bin/rake db:create                                            # Creates the database from DATABASE_URL or config/database.yml for the current RAILS_ENV (use db:cre...
bin/rake db:drop                                              # Drops the database from DATABASE_URL or config/database.yml for the current RAILS_ENV (use db:drop:...
bin/rake db:environment:set                                   # Set the environment value for the database
bin/rake db:fixtures:load                                     # Loads fixtures into the current environment's database
bin/rake db:load_config                                       # Send a test event to the remote Sentry server
bin/rake db:migrate                                           # Migrate the database (options: VERSION=x, VERBOSE=false, SCOPE=blog)
bin/rake db:migrate:status                                    # Display status of migrations
bin/rake db:rollback                                          # Rolls the schema back to the previous version (specify steps w/ STEP=n)
bin/rake db:schema:cache:clear                                # Clears a db/schema_cache.yml file
bin/rake db:schema:cache:dump                                 # Creates a db/schema_cache.yml file
bin/rake db:schema:dump                                       # Creates a db/schema.rb file that is portable against any DB supported by Active Record
bin/rake db:schema:load                                       # Loads a schema.rb file into the database
bin/rake db:seed                                              # Loads the seed data from db/seeds.rb
bin/rake db:setup                                             # Creates the database, loads the schema, and initializes with the seed data (use db:reset to also dr...
bin/rake db:structure:dump                                    # Dumps the database structure to db/structure.sql
bin/rake db:structure:load                                    # Recreates the databases from the structure.sql file
bin/rake db:version                                           # Retrieves the current schema version number
bin/rake dev:cache                                            # Toggle development mode caching on/off
bin/rake fix_timestamps_of_migrated_dossiers:run              # Fix the timestamps of dossiers affected by the faulty PJ migration
bin/rake geocode:all                                          # Geocode all objects without coordinates
bin/rake geocoder:maxmind:geolite:download                    # Download MaxMind GeoLite City data
bin/rake geocoder:maxmind:geolite:extract                     # Extract (unzip) MaxMind GeoLite City data
bin/rake geocoder:maxmind:geolite:insert                      # Load/refresh MaxMind GeoLite City data
bin/rake geocoder:maxmind:geolite:load                        # Download and load/refresh MaxMind GeoLite City data
bin/rake graphql:pro:validate[gem_version]                    # Get the checksum of a graphql-pro version and compare it to published versions on GitHub and graphq...
bin/rake graphql:schema:dump                                  # Dump the schema to JSON and IDL
bin/rake graphql:schema:idl                                   # Dump the schema to IDL in app/graphql/schema.graphql
bin/rake graphql:schema:json                                  # Dump the schema to JSON in app/graphql/schema.json
bin/rake haml:erb2haml                                        # Convert html.erb to html.haml each file in app/views
bin/rake initializers                                         # Print out all defined initializers in the order they are invoked by Rails
bin/rake jobs:check[max_age]                                  # Exit with error status if any jobs older than max_age seconds haven't been attempted yet
bin/rake jobs:clear                                           # Clear the delayed_job queue
bin/rake jobs:display_schedule                                # Display schedule for all cron jobs
bin/rake jobs:schedule                                        # Schedule all cron jobs
bin/rake jobs:work                                            # Start a delayed_job worker
bin/rake jobs:workoff                                         # Start a delayed_job worker and exit when all available jobs are complete
bin/rake log:clear                                            # Truncates all/specified *.log files in log/ to zero bytes (specify which logs with LOGS=test,develo...
bin/rake middleware                                           # Prints out your Rack middleware stack
bin/rake notes                                                # Enumerate all annotations (use notes:optimize, :fixme, :todo for focus)
bin/rake notes:custom                                         # Enumerate a custom annotation, specify with ANNOTATION=CUSTOM
bin/rake restart                                              # Restart app by touching tmp/restart.txt
bin/rake routes                                               # Print out all defined routes in match order, with names
bin/rake secret                                               # Generate a cryptographically secure secret key (this is typically used to generate a secret for coo...
bin/rake spec                                                 # Run all specs in spec directory (excluding plugin specs)
bin/rake spec:controllers                                     # Run the code examples in spec/controllers
bin/rake spec:features                                        # Run the code examples in spec/features
bin/rake spec:helpers                                         # Run the code examples in spec/helpers
bin/rake spec:jobs                                            # Run the code examples in spec/jobs
bin/rake spec:lib                                             # Run the code examples in spec/lib
bin/rake spec:mailers                                         # Run the code examples in spec/mailers
bin/rake spec:middlewares                                     # Run the code examples in spec/middlewares
bin/rake spec:models                                          # Run the code examples in spec/models
bin/rake spec:policies                                        # Run the code examples in spec/policies
bin/rake spec:serializers                                     # Run the code examples in spec/serializers
bin/rake spec:services                                        # Run the code examples in spec/services
bin/rake spec:views                                           # Run the code examples in spec/views
bin/rake stats                                                # Report code statistics (KLOCs, etc) from the application or engine
bin/rake superadmin:create[email]                             # Create a new super-admin account with the #EMAIL email address
bin/rake superadmin:delete[email]                             # Delete the #EMAIL super-admin account
bin/rake superadmin:list                                      # List all super-admins
bin/rake support:change_user_email                            # Change a user’s mail from OLD_EMAIL to NEW_EMAIL
bin/rake support:delete_user_account                          # Delete the user account for a given USER_EMAIL on the behalf of ADMIN_EMAIL
bin/rake support:update_dossier_siret                         # Change the SIRET for a given dossier (specified by DOSSIER_ID)
bin/rake test                                                 # Runs all tests in test folder except system ones
bin/rake test:db                                              # Run tests quickly, but also reset db
bin/rake test:system                                          # Run system tests only
bin/rake time:zones[country_or_offset]                        # List all time zones, list by two-letter country code (`rails time:zones[US]`), or list by UTC offse...
bin/rake tmp:clear                                            # Clear cache, socket and screenshot files from tmp/ (narrow w/ tmp:cache:clear, tmp:sockets:clear, t...
bin/rake tmp:create                                           # Creates tmp directories for cache, sockets, and pids
bin/rake webpacker                                            # Lists all available tasks in Webpacker
bin/rake webpacker:binstubs                                   # Installs Webpacker binstubs in this application
bin/rake webpacker:check_binstubs                             # Verifies that webpack & webpack-dev-server are present
bin/rake webpacker:check_node                                 # Verifies if Node.js is installed
bin/rake webpacker:check_yarn                                 # Verifies if Yarn is installed
bin/rake webpacker:clean[keep,age]                            # Remove old compiled webpacks
bin/rake webpacker:clobber                                    # Remove the webpack compiled output directory
bin/rake webpacker:compile                                    # Compile JavaScript packs using webpack for production with digests
bin/rake webpacker:info                                       # Provide information on Webpacker's environment
bin/rake webpacker:install                                    # Install Webpacker in this application
bin/rake webpacker:install:angular                            # Install everything needed for Angular
bin/rake webpacker:install:coffee                             # Install everything needed for Coffee
bin/rake webpacker:install:elm                                # Install everything needed for Elm
bin/rake webpacker:install:erb                                # Install everything needed for Erb
bin/rake webpacker:install:react                              # Install everything needed for React
bin/rake webpacker:install:stimulus                           # Install everything needed for Stimulus
bin/rake webpacker:install:svelte                             # Install everything needed for Svelte
bin/rake webpacker:install:typescript                         # Install everything needed for Typescript
bin/rake webpacker:install:vue                                # Install everything needed for Vue
bin/rake webpacker:verify_install                             # Verifies if Webpacker is installed
bin/rake webpacker:yarn_install                               # Support for older Rails versions
bin/rake yarn:install                                         # Install all JavaScript dependencies as specified via Yarn


```
