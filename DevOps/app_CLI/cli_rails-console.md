# bin/rails console

Certaines opérations pour les exploitants se font depuis la `console rails`

## Documentation
- [Rails Command Line: `rails console`](https://guides.rubyonrails.org/command_line.html#rails-console)

## Ouvrir la console rails

```bash
cd <path_demarchesSimplifiees>
bin/rails console
```                

Passe le terminal en **console Rails** qui donne accès à l'environnement rails, identifiable par `irb`.
irb pour "Interactive Ruby".

```
Running via Spring preloader in process 5780
Loading development environment (Rails 5.2.3)
irb(main):001:0>
```

Pour en sortir un simple exit suffit : 

```
irb(main):002:0> exit
```

## Actions de la console Rails spécifiques au logiciel _Démarches-Simplifiées_ 

à noter : les 2 actions suivantes sont possibles directement dans l'application pour le compte [**Super-Admin**](../app_Super-Admin/)
- Créer ou promouvoir un _instructeur_
- Créer ou promouvoir un _administrateur_

L'usage de la console rails pour ces 2 actions est utile quand le compte **Super-Admin** n'est pas disponible ; 
par exemple, quand l'URL de l'application n'est pas accessible sur Internet, l'authentififcation Github du compte  
**Super-Admin** ne semble pas fonctionner 
(test effectué avec [vagrant-dev-ds](https://gitlab.adullact.net/demarches-simplifiees/vagrant-dev-demarches-simplifiees)).

### Créer ou promouvoir un _instructeur_


```bash
cd <path_demarchesSimplifiees>
bin/rails console
```  

```ruby
User.create_or_promote_to_instructeur('jean_dupond@example.org', 'ceci est un mot de passe')
```

### Créer ou promouvoir un _administrateur_

```bash
cd <path_demarchesSimplifiees>
bin/rails console
```  

```ruby
User.create_or_promote_to_administrateur('jean_dupond@example.org', 'ceci est un mot de passe')
```

