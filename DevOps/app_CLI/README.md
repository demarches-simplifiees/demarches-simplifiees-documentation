# Outils en lignes de commande (CLI)

- [bin/rake](cli_rake.md)
- [bin/rails](cli_rails.md)
  - [bin/rails console](cli_rails-console.md)


## Mise à jour de l'application

> source: [betagouv/demarches-simplifiees.fr](https://github.com/betagouv/demarches-simplifiees.fr#mise-%C3%A0-jour-de-lapplication)
>
> Pour mettre à jour l'environnement de développement, 
> installer les nouvelles dépendances et faire jouer les migrations, exécutez :

```bash
cd <path_demarchesSimplifiees>
bin/update
```
