# Base de données

### Tables des utilisateurs

* **users** : contient tous les utilisateurs
* **super_admins** : contient tous les utilisateurs ayant les droits superAdmin
* **administrateurs** : contient tous les utilisateurs ayant les droits "administrateur" (création de formulaire)
* **instructeurs** : contient tous les utilisateurs ayant les droits instructeurs (traitement des dossiers) 
* **individuals** : contient tous les utilisateurs "citoyen" ou personne morale (usager remplissant une démarche en ligne)


```bash
# Se connecter à la base de données
psql <databaseName>

```
```sql
-- Afficher tous les utilisateurs existant
SELECT *
FROM users;

-- Afficher tous les utilisateurs avec droit SuperAdmin
SELECT *
FROM super_admins;

```
