# Services externes

Liste des services externes utilisés par le logiciel "Démarches-Simplifiées" :

## Services externes "obligatoires"

- [Helpscout](Helpscout.md) nécessaire pour les formulaire de contact (`/contact` et `/contact-admin`) 
- [Sendinblue.com](https://fr.sendinblue.com/) ou [Mailjet.com](https://fr.mailjet.com/)
  - nécessaire pour envoyer les mails aux utilisateurs (création de compte, suivi d'un dossier, ...)
- **OpenStack** "Object Storage" / d'OVH ?
  - nécessaire pour stocker les pièces jointes aux dossiers
  - nécessaire pour démarrer l'application en mode "production"
  - URL d'OVH codé en dur dans [config/storage.yml](https://github.com/betagouv/demarches-simplifiees.fr/blob/dev/config/storage.yml) ---> voir [Configuration openstack : OVH obligatoire](https://gitlab.adullact.net/demarches-simplifiees/demarches-simplifiees-global/-/issues/18)


## Services externes utiles

- logiciel libre ([Cachet](https://github.com/CachetHQ/Cachet), ...) ou service ([updown.io](https://updown.io/), [uptimerobot.com](https://uptimerobot.com/), ...)
  - nécessaire pour le lien en pied de page "Disponibilité"
- [API FranceConnect](https://franceconnect.gouv.fr/)
  - pour l'authentification des utilisateurs / utilisatrices
- [API Entreprise](https://api.gouv.fr/les-api/api-entreprise#api-description)
  - pour récupérer les informations sur une personne morale (raison sociale, adresse du siège, etc.) à partir du n° SIRET

### API France Connect

- https://partenaires.franceconnect.gouv.fr/documentation
- https://api.gouv.fr/les-api/franceconnect#api-description
- Fournisseur de service :
  - https://partenaires.franceconnect.gouv.fr/fcp/fournisseur-service
  - https://fournisseur-de-service.dev-franceconnect.fr/
  - https://github.com/france-connect/service-provider-example/
  - https://github.com/france-connect/identity-provider-example/blob/master/database.csv


## Autres services externes utilisés et coder en dur dans l'application
- [skylight.io](https://www.skylight.io) : service de monitoring Rails
- **livestorm** : liens "Participer à notre démonstration en ligne" et "Inscription à notre webinaire"
- **calendly.com** : lien  "prendre un rendez-vous téléphonique avec nous" 
- **vimeo.com** : lien  "visionner cette vidéo"
