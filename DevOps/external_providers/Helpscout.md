# Helpscout

[Helpscout.com](https://www.helpscout.com/)
est nécessaire pour les formulaire de contact (`/contact` et `/contact-admin`) 

Voir ticket [#6950 - ETQ mainteneur d'instance, je souhaite configurer Helpscout (documentation) ](https://github.com/betagouv/demarches-simplifiees.fr/issues/6950) sur le dépôt Github de DS.


## Contexte

Si nous souhaitons utiliser **Helpscout** sur une **instance DS**,  
nous ne trouvons pas de documentation officiel détaillant : 
- les éléments à configurer sur le service  [helpscout.com](https://www.helpscout.com/) 
- la correspondance des informations entre le logiciel **DS** et le service **Helpscout**.

Ci-dessous, tous les éléments que nous avons identifiés à ce sujet. 
Nous sommes preneurs d'information complémentaire pour valider et corriger ces éléments

## TODO

- [x] créer le compte https://www.helpscout.com/
- [x] identifier la configuration **DS** à modifier
- [x] identifier la configuration à mettre en place sur **helpscout**
  - [x] Helpscout "Mailbox"  
    - créer une nouvelle "Mailbox"
    - personnalisé l'adresse mail associé à cette "Mailbox" Helpscout
    - pour le fichier `.env` DS : récupérer l'ID de la "Mailbox" dans l'URL 
  - [x] Helpscout "User App"
    - créer une nouvelle "My App"
    - fournir `Redirection URL` qui doit à priori pointer sur DS
    - pour le fichier `.env` DS : récupérer `App ID` et `App Secret` généré par Helpscout
  - [x] Helpscout "Apps" 
     - créer une nouvelle "Custom App"
     - choisir "Dynamic content" pour "Content Type"
     - fournir "Callback Url" et "Secret Key"
- [x] identifier les relations entre les informations Helpscout et DS
  - [x] identifier les relations possibles entre les informations Helpscout et DS
  - [x] vérifier si il n'y a pas d'erreur ou d'information complémentaire à ajouter.
  
  
##  Logiciel DS

### URL dans DS

voir [config/routes.rb](https://github.com/betagouv/demarches-simplifiees.fr/blob/main/config/routes.rb#L169)

```ruby
  post "webhooks/helpscout", to: "webhook#helpscout"
  match "webhooks/helpscout", to: lambda { |_| [204, {}, nil] }, via: :head
```

L'URL `https://ds.exemple.org/webhooks/helpscout` utilisée sur **Helpscout** dans `Manage` > `Apps` > `Build a Custom App`

### Configuration DS

voir [config/env.example](https://github.com/betagouv/demarches-simplifiees.fr/blob/main/config/env.example)

```bash
# External service: integration with HelpScout (optional)
HELPSCOUT_MAILBOX_ID=""      # Mandatory for contact form
HELPSCOUT_CLIENT_ID=""       # Mandatory for contact form
HELPSCOUT_CLIENT_SECRET=""   # Mandatory for contact form
HELPSCOUT_WEBHOOK_SECRET=""  # Used for some functionality. It is not necessary for contact form 

# Salt for invisible_captcha session data.
# Must be the same value for all app instances behind a load-balancer.
INVISIBLE_CAPTCHA_SECRET="kikooloool"
```
`INVISIBLE_CAPTCHA_SECRET` est utilisé sur le formulaire de contact ---> voir https://github.com/betagouv/demarches-simplifiees.fr/pull/6786

## Service "Helpscout"

### Helpscout "Mailbox"

Créer une nouvelle "Mailbox" 
- Sur Helpscout :  `Manage` > `Mailboxs` > `New Mailbox`
- URL:  https://secure.helpscout.net/settings/mailboxes
  
Afficher la configuration de la "Mailbox"
- Sur Helpscout : `Manage` > `Mailboxs` > `<mailboxName>`
- URL : `https://secure.helpscout.net/settings/mailbox/<helpscoutMailboxId>/`
- Pour le fichier `.env` DS : récupérer l'ID de la "Mailbox" dans l'URL 

| Source      | Config Helpscout                                | équivalence DS                                            |   
| -------------- | ------------------------------------------------ | --------------------------------------------------------- |
| Helpscout  | dans l'URL `<helpscoutMailboxId>`  | **(1)** `.env` `HELPSCOUT_MAILBOX_ID` |

> Remarque **(1)** 
> La variable d'environnement  `HELPSCOUT_MAILBOX_ID` 
> est obligatoire pour que le formulaire de contact DS fonctionne.

### Helpscout "User App"


- URL : `https://secure.helpscout.net/users/apps/<helpscoutUserId>/`
- Sur Helpscout :  `Manage` > `Users` > `<userName>` > `My Apps` > `Create My App`
  - créer une nouvelle "My App"
  - fournir `Redirection URL` qui doit à priori pointer sur DS
  - pour le fichier `.env` DS : récupérer `App ID` et `App Secret` généré par Helpscout

| Source      | Config Helpscout     | équivalence DS                                                       | 
| -------------- | ------------------------- | ------------------------------------------------------------------- |
| Helpscout  | `App ID`                  | **(1)** `.env` `HELPSCOUT_CLIENT_ID`               |
| Helpscout  | `App Secret`           | **(1)** `.env` `HELPSCOUT_CLIENT_SECRET`    |
| DS             | `Redirection URL`   |  **(2)** `https://ds.exemple.org/contact`                   |


> Remarque **(1)** 
> Les variables d'environnement  `HELPSCOUT_CLIENT_ID` et  `HELPSCOUT_CLIENT_SECRET` 
> sont obligatoires pour que le formulaire de contact DS fonctionne. 
> 
> Remarque **(2)**
> Cette URL "obligatoire" (`Redirection URL`)  dans Helpscout, 
> ne semble pas avoir d'interaction avec DS.  (**TODO** : à vérifier)

Nota : visible uniquement pour le compte principal Helpscout (`Account Owner`) 
![Screenshot 2022-02-11 at 11-36-18 My Apps - ds Adullact](./images/hepscout/Helpscout_user-app.jpeg)

ressource complémentaire :
https://developer.helpscout.com/mailbox-api/overview/authentication/#oauth2-application

> You need to create an OAuth2 application before you can use the API. Create one by navigating to Your Profile > My apps and click Create My App. A redirection URL is necessary when using the Authorization Code flow.



### Helpscout "Apps"

- Gestion : https://secure.helpscout.net/apps/
- Création : https://secure.helpscout.net/apps/custom/
  - Sur Helpscout :  `Manage` > `Apps` > `Build a Custom App`
  - choisir "Dynamic content" pour "Content Type"
  - fournir "Callback Url" et "Secret Key"

| Source        | Config Helpscout  | équivalence DS                                                                 | 
| --------------- | ------------------------ | -------------------------------------------------------------------------- |
| DS              | `Secret Key`          | `.env` `HELPSCOUT_WEBHOOK_SECRET`   **(1)**     |
| DS              | `Callback Url`        | `https://ds.exemple.org/webhooks/helpscout`                  |

> Remarque **(1)** 
> La variable d'environnement  `HELPSCOUT_WEBHOOK_SECRET` 
> -  n'est pas nécessaire pour le formulaire de contact 
> - est  utilisé pour certaines fonctionnalités.  (**TODO** : à compléter)

![Screenshot 2022-02-10 at 16-38-43 Apps - ADULLACT Custom App TEST](./images/hepscout/Helpscout_custom-App.png)

##  Logs DS

Log de DS et configuration HELPSCOUT dans DS 
pour utiliser uniquement le formulaire de contact. 

### Sans configuration HELPSCOUT dans DS

Fichier **.env**
```bash
# External service: integration with HelpScout (optional)
HELPSCOUT_MAILBOX_ID=""
HELPSCOUT_CLIENT_ID=""
HELPSCOUT_CLIENT_SECRET=""
HELPSCOUT_WEBHOOK_SECRET=""
```

Logs quand le formulaire de contact est utilisé :
```ruby
Started POST "/contact" for <ip>
Processing by SupportController#create as HTML

Parameters:
{"authenticity_token"=>"[FILTERED]", "email"=>"test@example.org", "type"=>"", "ehwyvmqzpxokdcbg-fu"=>"", "spinner"=>"8b4c49baa4b7ddbf7e41f6791cfed49a", "dossier_id"=>"", "subject"=>"test sujet", "text"=>"test msg", "tags"=>"", "button"=>""}
ETHON: Libcurl initialized
ETHON: performed EASY effective_url=https://api.helpscout.net/v2/oauth2/token response_code=400 return_code=ok total_time=0.558917
ETHON: performed EASY effective_url=https://api.helpscout.net/v2/conversations response_code=401 return_code=ok total_time=0.153958
```
- La requête DS vers `https://api.helpscout.net/v2/oauth2/token` retourne un code HTTP 400  ---> Bad Request 
- La requête DS vers `https://api.helpscout.net/v2/conversations` retourne un code HTTP 401  ---> Unauthorized


### Avec une mauvaise configuration de HELPSCOUT dans DS

Ici, la configuration pour HelpScout a bien été ajouté, 
mais il y a une erreur de valeur pour `HELPSCOUT_MAILBOX_ID`.

Fichier **.env**
```bash
# External service: integration with HelpScout (optional)
HELPSCOUT_MAILBOX_ID="123_bad_mailbox_id_123"
HELPSCOUT_CLIENT_ID="zLgfI8..............................."
HELPSCOUT_CLIENT_SECRET="KTMaKe..............................."
HELPSCOUT_WEBHOOK_SECRET=""
```

Logs si j'utilise le formulaire de contact :
```ruby
Started POST "/contact" for <ip>
Processing by SupportController#create as HTML

Parameters:
{"authenticity_token"=>"[FILTERED]", "email"=>"test@example.org", "type"=>"other", "vfgba-xeph"=>"", "spinner"=>"bf0ad227d07b28126babcc4772431cf9", "dossier_id"=>"", "subject"=>"test sujet", "text"=>"test message", "tags"=>"", "button"=>""}
ETHON: Libcurl initialized
ETHON: performed EASY effective_url=https://api.helpscout.net/v2/oauth2/token response_code=200 return_code=ok total_time=0.57221
ETHON: performed EASY effective_url=https://api.helpscout.net/v2/conversations response_code=400 return_code=ok total_time=0.270621
```

- La requête DS vers `https://api.helpscout.net/v2/oauth2/token` retourne un code HTTP 200
- La requête DS vers `https://api.helpscout.net/v2/conversations` retourne un code HTTP 400   ---> Bad Request 


### Avec une configuration correcte de HELPSCOUT dans DS

Fichier **.env**
```bash
# External service: integration with HelpScout (optional)
HELPSCOUT_MAILBOX_ID="12345"
HELPSCOUT_CLIENT_ID="zLgfI8..............................."
HELPSCOUT_CLIENT_SECRET="KTMaKe..............................."
HELPSCOUT_WEBHOOK_SECRET=""
```

Logs si j'utilise le formulaire de contact :
```ruby
Started POST "/contact" for <ip>
Processing by SupportController#create as HTML

Parameters:
{"authenticity_token"=>"[FILTERED]", "email"=>"test@example.org", "type"=>"other", "vfgba-xeph"=>"", "spinner"=>"bf0ad227d07b28126babcc4772431cf9", "dossier_id"=>"", "subject"=>"test sujet", "text"=>"test message", "tags"=>"", "button"=>""}
ETHON: Libcurl initialized
ETHON: performed EASY effective_url=https://api.helpscout.net/v2/oauth2/token response_code=200 return_code=ok
ETHON: performed EASY effective_url=https://api.helpscout.net/v2/conversations response_code=201 return_code=ok
ETHON: performed EASY effective_url=https://api.helpscout.net/v2/conversations/<id>/tags response_code=204 return_code=ok
Redirected to https://ds.example.org/?formulaire_contact_general_submitted=true
```
- La requête DS vers `https://api.helpscout.net/v2/oauth2/token` retourne un code HTTP 200
- La requête DS vers `https://api.helpscout.net/v2/conversations` retourne un code HTTP 201
- La requête DS vers `https://api.helpscout.net/v2/conversations/<id>/tags` retourne un code HTTP 204

