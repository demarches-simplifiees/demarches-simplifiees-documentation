# Environnements

Plusieurs environnements avec des comportements très différents (envoi de mails, https, ...)
- [environment "development"](development/)
  - [config/environments/development.rb](https://github.com/betagouv/demarches-simplifiees.fr/blob/dev/config/environments/development.rb)
- [environment "production"](production/)
  - [config/environments/production.rb](https://github.com/betagouv/demarches-simplifiees.fr/blob/dev/config/environments/production.rb)


Ressources :
- [Documentation - Rails Environment Settings](https://guides.rubyonrails.org/configuring.html#rails-environment-settings)
- [Documentation - Creating Rails Environments](https://guides.rubyonrails.org/configuring.html#creating-rails-environments)
> By default Rails ships with three environments: "development", "test", and "production". While these are sufficient for most use cases, there are circumstances when you want more environments.
> 
> Imagine you have a server which mirrors the production environment but is only used for testing. Such a server is commonly called a "staging server". To define an environment called "staging" for this server, just create a file called config/environments/staging.rb. Please use the contents of any existing file in config/environments as a starting point and make the necessary changes from there.
> 
> That environment is no different than the default ones, start a server with rails server -e staging, a console with rails console -e staging, Rails.env.staging? works, etc.


## Documentation pour démarrer le serveur PUMA

### Documentation de la commande `bin/rails server`

```bash
bin/rails server --help
Usage:
  rails server -u [thin/puma/webrick] [options]

Options:
  -e, [--environment=ENVIRONMENT]  # Specifies the environment to run this server under (test/development/production).
  -p, [--port=port]                # Runs Rails on the specified port - defaults to 3000.
  -b, [--binding=IP]               # Binds Rails to the specified IP - defaults to 'localhost' in development and '0.0.0.0' in other environments'.
  -c, [--config=file]              # Uses a custom rackup configuration.
                                   # Default: config.ru
  -d, [--daemon], [--no-daemon]    # Runs server as a Daemon.
  -u, [--using=name]               # Specifies the Rack server used to run the application (thin/puma/webrick).
  -P, [--pid=PID]                  # Specifies the PID file.
                                   # Default: tmp/pids/server.pid
  -C, [--dev-caching], [--no-dev-caching]      # Specifies whether to perform caching in development.
      [--early-hints], [--no-early-hints]      # Enables HTTP/2 early hints.
      [--log-to-stdout], [--no-log-to-stdout]  # Whether to log to stdout. Enabled by default in development when not daemonized.
```

### Démarrer en mode "development" 

```bash
# La commande pour lancer le serveur PUMA en mode development 
bin/rails server

# équivalent à :
bin/rails server --environment=development 

# équivalent à :
bin/rails server --environment=development --binding=localhost
```

### Démarrer en mode "production" 
```bash
# La commande pour lancer le serveur PUMA en mode production
bin/rails server --environment=production

# équivalent à :
bin/rails server --environment=production --binding=0.0.0.0
    # L'IP 0.0.0.0 signifie toutes les adresses IPv4 de la machine locale
```

### Vérifier l'environnement utilisé par le serveur PUMA déjà démarré

```bash
bin/rails about  # Version numbers for Ruby, RubyGems, Rails, 
                 # application's folder, current Rails environment name, database adapter, schema version

   # about your application's environment
   # Rails version             6.0.3.2
   # Ruby version              ruby 2.6.5p114 (2019-10-01 revision 67812) [x86_64-linux]
   # RubyGems version          3.0.3
   # Rack version              2.2.3
   # JavaScript Runtime        Node.js (V8)
   # Middleware                Webpacker::DevServerProxy, Raven::Rack, Rack::MiniProfiler, ActionDispatch::HostAuthorization, Rack::Sendfile, ActionDispatch::Static, ActionDispatch::Executor, ActiveSupport::Cache::Strategy::LocalCache::Middleware, Rack::Runtime, Rack::MethodOverride, ActionDispatch::RequestId, RequestStore::Middleware, ActionDispatch::RemoteIp, Sprockets::Rails::QuietAssets, Rails::Rack::Logger, ActionDispatch::ShowExceptions, WebConsole::Middleware, ActionDispatch::DebugExceptions, ActionDispatch::ActionableExceptions, ActionDispatch::Reloader, ActionDispatch::Callbacks, ActiveRecord::Migration::CheckPending, ActionDispatch::Cookies, ActionDispatch::Session::CookieStore, ActionDispatch::Flash, ActionDispatch::ContentSecurityPolicy::Middleware, Rack::Head, Rack::ConditionalGet, Rack::ETag, Rack::TempfileReaper, Warden::Manager, Rack::Attack, Flipper::Middleware::Memoizer, Rack::Attack, OmniAuth::Strategies::GitHub, Xray::Middleware
   # Application root          /shared_dev
-> # Environment               development
   # Database adapter          postgresql
   # Database schema version   20200722135121
```

### Remarque sur la variable d'environnement `ENV["RAILS_ENV"]`

Dans la [documentation sur les environnements Rails](https://guides.rubyonrails.org/configuring.html#rails-environment-settings) est évoqué la variable d'environnement `ENV["RAILS_ENV"]`.

à ce jour, dans le vagrant v1, cette variable d'environnement ne semble pas modifier le lancement du serveur PUMA.

```bash
ENV["RAILS_ENV"]="production"
bin/rails server
bin/rails about 
   # about your application's environment
   #  (...)
   # Application root          /shared_dev
-> # Environment               development
```
